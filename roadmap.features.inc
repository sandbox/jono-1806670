<?php
/**
 * @file
 * roadmap.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function roadmap_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function roadmap_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function roadmap_node_info() {
  $items = array(
    'design_element' => array(
      'name' => t('Design Element'),
      'base' => 'node_content',
      'description' => t('Use this to describe major features of a project.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'issue' => array(
      'name' => t('Issue'),
      'base' => 'node_content',
      'description' => t('Use this to describe an issue, task, feature, or bug relating to a system version.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'project' => array(
      'name' => t('Project'),
      'base' => 'node_content',
      'description' => t('A project, that has versions and issues to track.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'version' => array(
      'name' => t('Version'),
      'base' => 'node_content',
      'description' => t('A version of a project, or release.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implements hook_workflow_features_default_workflow().
 */
function roadmap_workflow_features_default_workflow() {
  return array(
    'Issue Workflow' => array(
      'name' => 'Issue Workflow',
      'tab_roles' => 'author,2',
      'options' => 'a:3:{s:16:"comment_log_node";i:1;s:15:"comment_log_tab";i:1;s:13:"name_as_title";i:1;}',
      'states' => array(
        0 => array(
          'state' => '(creation)',
          'weight' => '-50',
          'sysid' => '1',
          'status' => '1',
        ),
        1 => array(
          'state' => 'Open',
          'weight' => '0',
          'sysid' => '0',
          'status' => '1',
        ),
        2 => array(
          'state' => 'Needs Definition/Research',
          'weight' => '0',
          'sysid' => '0',
          'status' => '1',
        ),
        3 => array(
          'state' => 'Accepted',
          'weight' => '1',
          'sysid' => '0',
          'status' => '1',
        ),
        4 => array(
          'state' => 'In Development',
          'weight' => '2',
          'sysid' => '0',
          'status' => '1',
        ),
        5 => array(
          'state' => 'Needs Review/Testing',
          'weight' => '4',
          'sysid' => '0',
          'status' => '1',
        ),
        6 => array(
          'state' => 'Fixed',
          'weight' => '7',
          'sysid' => '0',
          'status' => '1',
        ),
        7 => array(
          'state' => 'Closed',
          'weight' => '9',
          'sysid' => '0',
          'status' => '1',
        ),
        8 => array(
          'state' => 'Duplicate',
          'weight' => '10',
          'sysid' => '0',
          'status' => '1',
        ),
        9 => array(
          'state' => 'Will Not Do',
          'weight' => '10',
          'sysid' => '0',
          'status' => '1',
        ),
      ),
      'transitions' => array(
        0 => array(
          'sid' => '(creation)',
          'target_sid' => 'Accepted',
          'roles' => ',3,5',
        ),
        1 => array(
          'sid' => '(creation)',
          'target_sid' => 'In Development',
          'roles' => ',3',
        ),
        2 => array(
          'sid' => '(creation)',
          'target_sid' => 'Needs Review/Testing',
          'roles' => ',3',
        ),
        3 => array(
          'sid' => '(creation)',
          'target_sid' => 'Fixed',
          'roles' => ',3',
        ),
        4 => array(
          'sid' => '(creation)',
          'target_sid' => 'Closed',
          'roles' => ',3',
        ),
        5 => array(
          'sid' => '(creation)',
          'target_sid' => 'Duplicate',
          'roles' => ',3,5',
        ),
        6 => array(
          'sid' => '(creation)',
          'target_sid' => 'Will Not Do',
          'roles' => ',3,5',
        ),
        7 => array(
          'sid' => '(creation)',
          'target_sid' => 'Open',
          'roles' => 'author,2',
        ),
        8 => array(
          'sid' => '(creation)',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '2',
        ),
        9 => array(
          'sid' => 'Open',
          'target_sid' => 'Accepted',
          'roles' => '3,5',
        ),
        10 => array(
          'sid' => 'Open',
          'target_sid' => 'In Development',
          'roles' => '3',
        ),
        11 => array(
          'sid' => 'Open',
          'target_sid' => 'Needs Review/Testing',
          'roles' => '3',
        ),
        12 => array(
          'sid' => 'Open',
          'target_sid' => 'Fixed',
          'roles' => '3',
        ),
        13 => array(
          'sid' => 'Open',
          'target_sid' => 'Closed',
          'roles' => '3',
        ),
        14 => array(
          'sid' => 'Open',
          'target_sid' => 'Duplicate',
          'roles' => '3,5',
        ),
        15 => array(
          'sid' => 'Open',
          'target_sid' => 'Will Not Do',
          'roles' => '3,5',
        ),
        16 => array(
          'sid' => 'Open',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '3,5,2',
        ),
        17 => array(
          'sid' => 'Needs Definition/Research',
          'target_sid' => 'Open',
          'roles' => '3,5,2',
        ),
        18 => array(
          'sid' => 'Needs Definition/Research',
          'target_sid' => 'Accepted',
          'roles' => '3,5',
        ),
        19 => array(
          'sid' => 'Needs Definition/Research',
          'target_sid' => 'Duplicate',
          'roles' => '3,5',
        ),
        20 => array(
          'sid' => 'Needs Definition/Research',
          'target_sid' => 'Will Not Do',
          'roles' => '3,5',
        ),
        21 => array(
          'sid' => 'Accepted',
          'target_sid' => 'In Development',
          'roles' => '2',
        ),
        22 => array(
          'sid' => 'Accepted',
          'target_sid' => 'Needs Review/Testing',
          'roles' => ',3,2',
        ),
        23 => array(
          'sid' => 'Accepted',
          'target_sid' => 'Fixed',
          'roles' => ',3,2',
        ),
        24 => array(
          'sid' => 'Accepted',
          'target_sid' => 'Closed',
          'roles' => ',3',
        ),
        25 => array(
          'sid' => 'Accepted',
          'target_sid' => 'Duplicate',
          'roles' => ',3,5',
        ),
        26 => array(
          'sid' => 'Accepted',
          'target_sid' => 'Will Not Do',
          'roles' => ',3,5',
        ),
        27 => array(
          'sid' => 'Accepted',
          'target_sid' => 'Open',
          'roles' => '3,5',
        ),
        28 => array(
          'sid' => 'Accepted',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '3,5,2',
        ),
        29 => array(
          'sid' => 'In Development',
          'target_sid' => 'Accepted',
          'roles' => ',3,5',
        ),
        30 => array(
          'sid' => 'In Development',
          'target_sid' => 'Needs Review/Testing',
          'roles' => '2,3',
        ),
        31 => array(
          'sid' => 'In Development',
          'target_sid' => 'Fixed',
          'roles' => ',3,2',
        ),
        32 => array(
          'sid' => 'In Development',
          'target_sid' => 'Closed',
          'roles' => ',3',
        ),
        33 => array(
          'sid' => 'In Development',
          'target_sid' => 'Duplicate',
          'roles' => ',3,5',
        ),
        34 => array(
          'sid' => 'In Development',
          'target_sid' => 'Will Not Do',
          'roles' => ',3,5',
        ),
        35 => array(
          'sid' => 'In Development',
          'target_sid' => 'Open',
          'roles' => '3,5',
        ),
        36 => array(
          'sid' => 'In Development',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '3,5',
        ),
        37 => array(
          'sid' => 'Needs Review/Testing',
          'target_sid' => 'Accepted',
          'roles' => ',5',
        ),
        38 => array(
          'sid' => 'Needs Review/Testing',
          'target_sid' => 'In Development',
          'roles' => '2',
        ),
        39 => array(
          'sid' => 'Needs Review/Testing',
          'target_sid' => 'Fixed',
          'roles' => '2',
        ),
        40 => array(
          'sid' => 'Needs Review/Testing',
          'target_sid' => 'Closed',
          'roles' => ',3,5',
        ),
        41 => array(
          'sid' => 'Needs Review/Testing',
          'target_sid' => 'Duplicate',
          'roles' => ',3,5',
        ),
        42 => array(
          'sid' => 'Needs Review/Testing',
          'target_sid' => 'Will Not Do',
          'roles' => ',3,5',
        ),
        43 => array(
          'sid' => 'Needs Review/Testing',
          'target_sid' => 'Open',
          'roles' => '5',
        ),
        44 => array(
          'sid' => 'Needs Review/Testing',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '5',
        ),
        45 => array(
          'sid' => 'Fixed',
          'target_sid' => 'Accepted',
          'roles' => ',3,5',
        ),
        46 => array(
          'sid' => 'Fixed',
          'target_sid' => 'In Development',
          'roles' => '2,3,5',
        ),
        47 => array(
          'sid' => 'Fixed',
          'target_sid' => 'Needs Review/Testing',
          'roles' => '2,3,5',
        ),
        48 => array(
          'sid' => 'Fixed',
          'target_sid' => 'Closed',
          'roles' => ',3,5',
        ),
        49 => array(
          'sid' => 'Fixed',
          'target_sid' => 'Duplicate',
          'roles' => ',3,5',
        ),
        50 => array(
          'sid' => 'Fixed',
          'target_sid' => 'Will Not Do',
          'roles' => ',3,5',
        ),
        51 => array(
          'sid' => 'Fixed',
          'target_sid' => 'Open',
          'roles' => '3,5',
        ),
        52 => array(
          'sid' => 'Fixed',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '3,5',
        ),
        53 => array(
          'sid' => 'Closed',
          'target_sid' => 'Accepted',
          'roles' => ',3,5',
        ),
        54 => array(
          'sid' => 'Closed',
          'target_sid' => 'In Development',
          'roles' => ',3,5',
        ),
        55 => array(
          'sid' => 'Closed',
          'target_sid' => 'Needs Review/Testing',
          'roles' => ',3,5',
        ),
        56 => array(
          'sid' => 'Closed',
          'target_sid' => 'Fixed',
          'roles' => ',3,5',
        ),
        57 => array(
          'sid' => 'Closed',
          'target_sid' => 'Duplicate',
          'roles' => ',3,5',
        ),
        58 => array(
          'sid' => 'Closed',
          'target_sid' => 'Will Not Do',
          'roles' => ',3,5',
        ),
        59 => array(
          'sid' => 'Closed',
          'target_sid' => 'Open',
          'roles' => '3,5',
        ),
        60 => array(
          'sid' => 'Closed',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '3,5',
        ),
        61 => array(
          'sid' => 'Duplicate',
          'target_sid' => 'Accepted',
          'roles' => ',3,5',
        ),
        62 => array(
          'sid' => 'Duplicate',
          'target_sid' => 'In Development',
          'roles' => ',3,5',
        ),
        63 => array(
          'sid' => 'Duplicate',
          'target_sid' => 'Needs Review/Testing',
          'roles' => ',3,5',
        ),
        64 => array(
          'sid' => 'Duplicate',
          'target_sid' => 'Fixed',
          'roles' => ',3,5',
        ),
        65 => array(
          'sid' => 'Duplicate',
          'target_sid' => 'Closed',
          'roles' => ',3,5',
        ),
        66 => array(
          'sid' => 'Duplicate',
          'target_sid' => 'Will Not Do',
          'roles' => ',3,5',
        ),
        67 => array(
          'sid' => 'Duplicate',
          'target_sid' => 'Open',
          'roles' => '3,5',
        ),
        68 => array(
          'sid' => 'Duplicate',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '3,5',
        ),
        69 => array(
          'sid' => 'Will Not Do',
          'target_sid' => 'Accepted',
          'roles' => ',3,5',
        ),
        70 => array(
          'sid' => 'Will Not Do',
          'target_sid' => 'In Development',
          'roles' => ',3,5',
        ),
        71 => array(
          'sid' => 'Will Not Do',
          'target_sid' => 'Needs Review/Testing',
          'roles' => ',3,5',
        ),
        72 => array(
          'sid' => 'Will Not Do',
          'target_sid' => 'Fixed',
          'roles' => ',3,5',
        ),
        73 => array(
          'sid' => 'Will Not Do',
          'target_sid' => 'Closed',
          'roles' => ',3,5',
        ),
        74 => array(
          'sid' => 'Will Not Do',
          'target_sid' => 'Duplicate',
          'roles' => '2,3,5',
        ),
        75 => array(
          'sid' => 'Will Not Do',
          'target_sid' => 'Open',
          'roles' => '3,5',
        ),
        76 => array(
          'sid' => 'Will Not Do',
          'target_sid' => 'Needs Definition/Research',
          'roles' => '3,5',
        ),
      ),
      'node_types' => array(
        0 => array(
          'type' => 'design_element',
          'wid' => '2',
        ),
        1 => array(
          'type' => 'issue',
          'wid' => '2',
        ),
      ),
    ),
  );
}
