<?php
/**
 * @file
 * roadmap.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function roadmap_user_default_permissions() {
  $permissions = array();

  // Exported permission: access draggableviews.
  $permissions['access draggableviews'] = array(
    'name' => 'access draggableviews',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'draggableviews',
  );

  // Exported permission: access workflow summary views.
  $permissions['access workflow summary views'] = array(
    'name' => 'access workflow summary views',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'workflow_views',
  );

  // Exported permission: administer workflow.
  $permissions['administer workflow'] = array(
    'name' => 'administer workflow',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'workflow_admin_ui',
  );

  // Exported permission: create design_element content.
  $permissions['create design_element content'] = array(
    'name' => 'create design_element content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create issue content.
  $permissions['create issue content'] = array(
    'name' => 'create issue content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create project content.
  $permissions['create project content'] = array(
    'name' => 'create project content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create version content.
  $permissions['create version content'] = array(
    'name' => 'create version content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any design_element content.
  $permissions['delete any design_element content'] = array(
    'name' => 'delete any design_element content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any issue content.
  $permissions['delete any issue content'] = array(
    'name' => 'delete any issue content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any project content.
  $permissions['delete any project content'] = array(
    'name' => 'delete any project content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any version content.
  $permissions['delete any version content'] = array(
    'name' => 'delete any version content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own design_element content.
  $permissions['delete own design_element content'] = array(
    'name' => 'delete own design_element content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own issue content.
  $permissions['delete own issue content'] = array(
    'name' => 'delete own issue content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own project content.
  $permissions['delete own project content'] = array(
    'name' => 'delete own project content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own version content.
  $permissions['delete own version content'] = array(
    'name' => 'delete own version content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any design_element content.
  $permissions['edit any design_element content'] = array(
    'name' => 'edit any design_element content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any issue content.
  $permissions['edit any issue content'] = array(
    'name' => 'edit any issue content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any project content.
  $permissions['edit any project content'] = array(
    'name' => 'edit any project content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any version content.
  $permissions['edit any version content'] = array(
    'name' => 'edit any version content',
    'roles' => array(
      0 => 'Project Manager',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own design_element content.
  $permissions['edit own design_element content'] = array(
    'name' => 'edit own design_element content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own issue content.
  $permissions['edit own issue content'] = array(
    'name' => 'edit own issue content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own project content.
  $permissions['edit own project content'] = array(
    'name' => 'edit own project content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own version content.
  $permissions['edit own version content'] = array(
    'name' => 'edit own version content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: execute views_bulk_operations_delete_item.
  $permissions['execute views_bulk_operations_delete_item'] = array(
    'name' => 'execute views_bulk_operations_delete_item',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: execute views_bulk_operations_modify_action.
  $permissions['execute views_bulk_operations_modify_action'] = array(
    'name' => 'execute views_bulk_operations_modify_action',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: execute workflow_select_given_state_action.
  $permissions['execute workflow_select_given_state_action'] = array(
    'name' => 'execute workflow_select_given_state_action',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: execute workflow_select_next_state_action.
  $permissions['execute workflow_select_next_state_action'] = array(
    'name' => 'execute workflow_select_next_state_action',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'actions_permissions',
  );

  // Exported permission: schedule workflow transitions.
  $permissions['schedule workflow transitions'] = array(
    'name' => 'schedule workflow transitions',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'workflow',
  );

  return $permissions;
}
