<?php
/**
 * @file
 * roadmap.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function roadmap_user_default_roles() {
  $roles = array();

  // Exported role: Project Manager.
  $roles['Project Manager'] = array(
    'name' => 'Project Manager',
    'weight' => '4',
  );

  return $roles;
}
