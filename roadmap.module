<?php
/**
 * @file
 * Code for the Project Manager feature.
 */

include_once 'roadmap.features.inc';

/**
 * Computed field for version string
 */
function computed_field_field_version_id_compute(&$entity_field, $entity_type, $entity, $field, $instance, $langcode, $items) {
  $numbers = array();
  $numbers[] = empty($entity->field_major['und'][0]['value']) ? 0 : $entity->field_major['und'][0]['value'];
  $numbers[] = empty($entity->field_minor['und'][0]['value']) ? 0 : $entity->field_minor['und'][0]['value'];
  if (!empty($entity->field_patch['und'][0]['value'])) {
    $numbers[] = $entity->field_patch['und'][0]['value'];
  }
  $term = taxonomy_term_load($entity->field_api_compatibility['und'][0]['tid']);
  $retval = array($term->name);
  $retval[] = implode(".", $numbers);
  if (!empty($entity->field_extra_tag['und'][0]['value'])) {
    $retval[] = $entity->field_extra_tag['und'][0]['value'];
  }
  $entity_field[0]['value'] = implode("-", $retval);
}

/**
 * Return display for version string
 */
function computed_field_field_version_id_display($field, $entity_field_item, $entity_lang, $langcode) {
  return $entity_field_item['value'];
}

/**
 * Implements hook_node_prepare().
 */
function roadmap_node_prepare($node) {
  // Set node title to a placeholder string
  if ($node->type == "version" && empty($node->title)) {
    $node->title = "(This will be set to the version id.)";
  }
}

/**
 * Implements hook_node_presave().
 */
function roadmap_node_presave($node) {
  // Set node title to the version id
  if ($node->type == "version" && !empty($node->field_version_id['und'][0]['value'])) {
    $project_nid = $node->field_project['und'][0]['target_id'];
    $project = node_load($project_nid);
    $project_title = empty($project->field_short_title['und'][0]['safe_value']) ? $project->title : $project->field_short_title['und'][0]['safe_value'];
    $node->title = $project_title . " " . $node->field_version_id['und'][0]['value'];
  }
}

/**
 * Implements hook_update_N().
 */
function roadmap_update_7100(&$sandbox) {

  $vocabs = taxonomy_get_vocabularies();
  $additions = array(
    'api_compatibility' => array('6.x', '7.x', '8.x'),
    'functional_area' => array('User'),
    'issue_type' => array('Bug', 'Feature', 'Support Request', 'Task'),
    'priority' => array('Critical', 'Normal', 'Minor'),
    'status' => array('Active', 'Closed', 'Duplicate', 'Needs Review/Testing', 'Fixed'),
    'technical_risk' => array('High', 'Medium', 'Low'),
  );

  $msgs = array();
  foreach ($vocabs as $vocab) {
    if (isset($additions[$vocab->machine_name])) {
      $existing_terms = taxonomy_get_tree($vocab->vid);
      $existing_term_names = array();
      foreach ($existing_terms as $term) {
        $existing_term_names[] = $term->name;
      }
      $weight = 0;
      $added = array();
      $existing = array();
      foreach ($additions[$vocab->machine_name] as $term_name) {
        if (!in_array($term_name, $existing_term_names)) {
          $term = new stdClass;
          $term->vid = $vocab->vid;
          $term->name = $term_name;
          $term->weight = $weight++;
          taxonomy_term_save($term);
          $added[] = $term->name;
        }
        else {
          $existing[] = $term_name;
        }
      }
      if (!empty($added)) $msgs[] = "{$vocab->name} terms added: " . implode(", ", $added);
      if (!empty($existing)) $msgs[] = "{$vocab->name} terms already exist: " . implode(", ", $existing);
    }
  }
  return implode("\n", $msgs);
}
